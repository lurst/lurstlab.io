#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Gil'
SITENAME = u'Gil Codes'

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = u'en'

THEME = 'theme'

DEFAULT_PAGINATION = 0

PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}.html'
