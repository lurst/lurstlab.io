Title: Home
URL:
Save_as: index.html

Welcome! Unlike a usual blog, you won't find a list of articles sorted by
published date, but instead you'll find a collection of pages which may
relate to each other.

Think of this as a live wiki kind of thing, I'll try to make sure there are
links for every page created.

Here is a list to get you started:

- [About me](/about)
