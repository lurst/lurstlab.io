Title: On Test-Driven Development
Save_as: tdd.html

I'm somewhat familiar with TDD, I've had some experience with it in the past
but it's something that is in my toolbox getting a bit of dust since I tend not
to use it mainly due to laziness and not being used to doing it.

I've recently re-started reading
[TDD with Python](http://www.obeythetestinggoat.com/) which is my only
technical book with an authors signature (given to me on PyCon Ireland) and it
has been helping me immensely. In it, the author (Harry Percival) talks about
the [Refactoring Cat](https://img.pandawhale.com/post-10513-Code-Refactoring-Cat-in-Bathtu-U295.gif)
vs the Testing Goat, where the Refactoring Cat represents making a lot of
changes everywhere and then checking if they are correct whereas the Testing
Goat represents making one small change at a time ensuring (with tests) that it
is the correct change.

My point with this is that even though the book also teaches a lot of django
and web development in general, at least to me, the best lesson is really in
disciplining myself as a developer to not be a refactoring cat which I admit to
being sometimes.

But the **real** point for me to write this is to commit myself to being more
disciplined, from this point on, I'll _try_ to use TDD for everything that I
do, and I'll try to update this page with some sort of frequency as I see fit
to make sure I'm going in the correct path.

I'll write updates bellow with the dates too.

*Written in* 31st May 2016.
